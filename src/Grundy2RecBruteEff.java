import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Grundy game
 * 
 * @author A.ROBIN and M.SEVETTE
 */

class Grundy2RecBruteEff{
    long cpt;
    
    void principal(){
        //testafficheJeu();
        //testPartieTerminee();
        //partie();
        testEstGagnanteEfficacite();
    }
    /**
     * Efficiency testing method of estGagnante
     */
    void testEstGagnanteEfficacite(){
        int i = 1;
        int n;
        long t1,t2,diffT;
        // n = 3
        n = 3;
        while( i <= 22){
            ArrayList<Integer> jeu = new ArrayList<Integer>();
            jeu.add(n);
            cpt = 0;
            t1 = System.nanoTime();
            estGagnante(jeu);
            t2 = System.nanoTime();
            diffT = (t2 - t1);//en nanosecondes
            System.out.println(i);
            System.out.println("Tps = " + diffT/1000000 + "ms");
            System.out.println("l'éfficacité est de " + cpt);
            i++;
            n = n + 1;
        }
    }
    
    /**
     * Method that manages the game by creating the players 
     * and the number of matche.
     */
    void partie(){
        String joueur1;
        String joueur2;
        int tour = 0;
        int premier = -1;
        int nbAllumette = SimpleInput.getInt("Nombre d'allumette : ");
        while(nbAllumette <= 2){
            nbAllumette = SimpleInput.getInt("Nombre d'allumette : ");
        }
        joueur1 = SimpleInput.getString("Nom du joueur  : ");
        joueur2 = "Ordi";
        while(premier != 0 && premier != 1){
                premier = SimpleInput.getInt
                ("Premier à jouer :\n- 0 l'ordinateur"+
                "\n- 1 le joueur\n");
        }
        if(premier == 0){
            joueur2 = joueur1;
            joueur1 = "Ordi";
        }
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        jeu.add(nbAllumette);
        while(!partieTerminee(jeu)){
                if (tour % 2 == 0){
                    if(premier == 1){
                    //odd turn it is therefore up to player 2 to play
                        untour(joueur1, jeu, tour);
                    }else{
                        unTourOrdi(joueur1, jeu, tour);
                    }
                }
                if(tour % 2 != 0){
                    //Even tour it is therefore up to the computer to play
                    if(premier == 1){
                        unTourOrdi(joueur2, jeu, tour);
                    }else{
                        untour(joueur2, jeu, tour);
                    }
                }
                tour++;
        }
        if(tour % 2 == 0){
            //If the turn is even then player 1 cannot play
            //and so it is player 2 who wins
            System.out.println(joueur2 + " a gagné.");
        }else{
            System.out.println(joueur1 + " a gagné.");
        }
    }

    /**
     * Method that displays matches
     * @param tour Tour to which we are
     * @param jeu ArrayList filled with matches
     */
    void afficheJeu(int tour, ArrayList<Integer> jeu){
        int i = 0;
        String str = " | ";
        while(i < jeu.size() && i <= tour){
            //repeat() method repeats
            //a number of times a String
            System.out.println
            (i + " : " + str.repeat(jeu.get(i)));
            i++;
        }
    }
    
    /** 
     * Method that checks after each turn that the game can or not continue
     * @param jeu ArrayList filled with matches
     * @return if the game is finish
     */
    boolean partieTerminee(ArrayList<Integer> jeu){
        boolean estTerminer = true;
        int i = 0;
        while(i < jeu.size() && estTerminer){
            if (jeu.get(i) > 2){
                estTerminer = false;
            }
            i++;
        }
        return estTerminer;
    }
    /**
	 * Teste la méthode partieTerminer()
	 */
	void testPartieTerminee(){
		System.out.println();
		System.out.println("***testpartieTerminer()");
        
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(1);
        jeu1.add(2);
        jeu1.add(3);
        jeu1.add(4);
		testCasPartieTerminee (jeu1,false);
        
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(1);
        jeu2.add(2);
        jeu2.add(1);
        jeu2.add(2);
		testCasPartieTerminee (jeu2,true);
        
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(3);
        jeu3.add(2);
        jeu3.add(1);
        jeu3.add(2);
		testCasPartieTerminee (jeu3,false);
        
        ArrayList<Integer> jeu4 = new ArrayList<Integer>();
        jeu4.add(1);
        jeu4.add(2);
        jeu4.add(1);
        jeu4.add(3);
		testCasPartieTerminee (jeu4,false);
        
        ArrayList<Integer> jeu5 = new ArrayList<Integer>();
        jeu5.add(1);
        jeu5.add(5);
        jeu5.add(1);
        jeu5.add(3);
		testCasPartieTerminee (jeu5,false);
        
        ArrayList<Integer> jeu6 = new ArrayList<Integer>();
        jeu6.add(5);
        jeu6.add(0);
        jeu6.add(0);
		testCasPartieTerminee (jeu6,false);
        
	}
	/**
	 * tests a call to partieTerminer
	 * @param jeu Game board
	 * @param result result expected
	 */
	void testCasPartieTerminee(ArrayList<Integer> jeu, boolean result){
		//Arrange
		System.out.print 
		("partieTerminer () \t= " + result + "\t : ");
		
		//Act
		boolean resExec = partieTerminee(jeu);
        
		//Assert
		if (resExec == result){
			System.out.println("OK");
		}else{
			System.out.println("ERREUR");
		}
	}
    
    /**
     * Method that spins a turn of the game for a given player
     * @param joueur name of the player
     * @param jeu ArrayList filled with matches
     * @param tour Tour to which we are
     */
    void untour(String joueur, ArrayList<Integer> jeu, int tour ){
        int ligne ;
        int nbAllumetteASeparer;
        afficheJeu(tour, jeu);
        System.out.println();
        System.out.println("Tour du joueur : " + joueur);
        ligne = SimpleInput.getInt("Ligne : ");
        //line between 0 and the turn
        while
        (ligne < 0 || ligne > tour || jeu.get(ligne) < 3){
            ligne = SimpleInput.getInt("Ligne : ");
        }
        nbAllumetteASeparer = SimpleInput.getInt
        ("Nombre d'allumettes à séparer : ");
        /**
         * Test if the number of matches is between 1 and the max number
         *  of the line - 1 and finally if the two heaps will be very 
         * different after separation.
         */
        while(nbAllumetteASeparer <= 0 ||
        nbAllumetteASeparer >= jeu.get(ligne) ||
        nbAllumetteASeparer ==  jeu.get(ligne) - nbAllumetteASeparer){
            nbAllumetteASeparer = SimpleInput.getInt
            ("Nombre d'allumettes à séparer : ");
        }
        enlever(jeu,ligne,nbAllumetteASeparer);
    }
    
    /**
     * Method that runs a round of the game for the computer
     * @param joueur name of the player
     * @param jeu ArrayList filled with matches
     * @param tour Tour to which we are
     */
    void unTourOrdi(String joueur, ArrayList<Integer> jeu, int tour){
        int ligne ;
        int nbAllumetteASeparer;
        afficheJeu(tour, jeu);
        System.out.println();
        System.out.println("Tour du joueur : " + joueur);
        if(jouerGagnant(jeu)){
        }else{
            ligne = (int)(Math.random() * tour);
            while(jeu.get(ligne) < 3){
                ligne = (int)(Math.random() * tour);
            }
            nbAllumetteASeparer = (int)(Math.random() * jeu.get(ligne)- 2) + 1;
            while
            (nbAllumetteASeparer == jeu.get(ligne) - nbAllumetteASeparer){
                nbAllumetteASeparer = (int)(Math.random() * jeu.get(ligne)- 2) + 1;
            }
            enlever(jeu,ligne,nbAllumetteASeparer);
        }
    }
    
    /**
     * testing method affichePartie
     */
    void testafficheJeu(){
        System.out.println();
		System.out.println("***testaffichePartie()");
        
        ArrayList<Integer>allumettes1 = new ArrayList<Integer>();
        allumettes1.add(5);
        allumettes1.add(0);
        String result1 = "0 :  |  |  |  |  | ";
		testCasAfficheJeu (0,allumettes1,result1);
        
        ArrayList<Integer>allumettes2 = new ArrayList<Integer>();
        allumettes2.add(5);
        allumettes2.add(4);
        allumettes2.add(0);
        String result2 = "0 :  |  |  |  |  | \n1 :  |  |  |  | ";
		testCasAfficheJeu (1,allumettes2,result2);
        
        ArrayList<Integer>allumettes3 = new ArrayList<Integer>();
        allumettes3.add(1);
        allumettes3.add(1);
        allumettes3.add(3);
        allumettes3.add(2);
        String result3 = "0 :  | \n1 :  | \n2 :  |  |  |\n3 :  |  | ";
		testCasAfficheJeu (3,allumettes3,result3);
    }
    /**
     * Test a call of affichePartie
     * @param tour Tour to which we are
     * @param jeu ArrayList filled with matches
     * @param result expected Result
     */
    void testCasAfficheJeu
    (int tour, ArrayList<Integer> jeu, String result){
        System.out.println("résultat :\n" + result);
        System.out.print("méthode:\n");
        afficheJeu(tour,jeu);
        System.out.println();
        
    }
    
	
    /**
     * Play the winning move if it exists
     * 
     * @param jeu Game board
     * @return true if there is a winning move, false otherwise
     */
    boolean jouerGagnant(ArrayList<Integer> jeu) {
        boolean gagnant = false;
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else {
            ArrayList<Integer> essai = new ArrayList<Integer>();
            int ligne = premier(jeu, essai);
			// mise en oeuvre de la règle numéro2
			// Une situation (ou position) est dite gagnante pour la machine (ou le joueur, peu importe), s’il existe AU MOINS UNE 
			// décomposition (c-à-d UNE action qui consiste à décomposer un tas en 2 tas inégaux) perdante pour l’adversaire.
            while (ligne != -1 && !gagnant) {
                if (estPerdante(essai)) {
                    jeu.clear();
                    gagnant = true;
                    for (int i = 0; i < essai.size(); i++) {
                        jeu.add(essai.get(i));
                    }
                } else {
                    ligne = suivant(jeu, essai, ligne);
                }
            }
        }
		
        return gagnant;
    }
	
	/**
     * RECURSIVE method that indicates whether the configuration (of the current game or trial set) loses
     * 
     * @param jeu Current game board (the state of the game at a certain point during the game)
     * @return true if the configuration (of the game) loses, false otherwise
     */
    boolean estPerdante(ArrayList<Integer> jeu) {
        boolean ret = true; // par défaut la configuration est perdante
		
        if (jeu == null) {
            System.err.println("estPerdante(): le paramètre jeu est null");
        }
		
		else {
			// si il n'y a plus que des tas de 1 ou 2 allumettes dans le plateau de jeu
			// alors la situation est forcément perdante (ret=true) = FIN de la récursivité
            if ( !estPossible(jeu) ) {
                ret = true;
            }
			
			else {
				// création d'un jeu d'essais qui va examiner toutes les décompositions
				// possibles à partir de jeu
                ArrayList<Integer> essai = new ArrayList<Integer>(); // size = 0
				
				// toute première décomposition : enlever 1 allumette au premier tas qui possède
				// au moins 3 allumettes, ligne = -1 signifie qu'il n'y a plus de tas d'au moins 3 allumettes
                int ligne = premier(jeu, essai);
                while ( (ligne != -1) && ret) {
					// mise en oeuvre de la règle numéro1
					// Une situation (ou position) est dite perdante pour la machine (ou le joueur, peu importe) si et seulement si TOUTES 
					// ses décompositions possibles (c-à-d TOUTES les actions qui consistent à décomposer un tas en 2 tas inégaux) sont 
					// TOUTES gagnantes pour l’adversaire.
					// Si UNE SEULE décomposition (à partir du jeu) est perdante (pour l'adversaire) alors la configuration n'EST PAS perdante.
					// Ici l'appel à "estPerdante" est RECURSIF.
                    if (estPerdante(essai) == true) {
					
                        ret = false;
						
                    } else {
						// génère la configuration d'essai suivante (c'est-à-dire UNE décomposition possible)
						// à partir du jeu, si ligne = -1 il n'y a plus de décomposition possible
                        ligne = suivant(jeu, essai, ligne);
                    }
                }
            }
        }
        cpt++;
        return ret;
    }
	
	/**
     * Indicates whether the configuration wins.
	 * Method that simply calls "estPerdante".
     * 
     * @param jeu Game board
     * @return true if the configuration is winning, false otherwise
     */
    boolean estGagnante(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estGagnante(): le paramètre jeu est null");
        } else {
            ret = !estPerdante(jeu);
        }
        return ret;
    }

    /**
     * Brief tests of the method joueurGagnant()
     */
    void testJouerGagnant() {
        System.out.println();
        System.out.println("*** testJouerGagnant() ***");

        System.out.println("Test des cas normaux");
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(6);
        ArrayList<Integer> resJeu1 = new ArrayList<Integer>();
        resJeu1.add(4);
        resJeu1.add(2);
		
        testCasJouerGagnant(jeu1, resJeu1, true);
        
    }

    /**
     * Test d'un cas de la méthode jouerGagnant()
	 *
	 * @param jeu le plateau de jeu
	 * @param resJeu le plateau de jeu après avoir joué gagnant
	 * @param res le résultat attendu par jouerGagnant
     */
    void testCasJouerGagnant(ArrayList<Integer> jeu, ArrayList<Integer> resJeu, boolean res) {
        // Arrange
        System.out.print("jouerGagnant (" + jeu.toString() + ") : ");

        // Act
        boolean resExec = jouerGagnant(jeu);

        // Assert
        System.out.print(jeu.toString() + " " + resExec + " : ");
        if (jeu.equals(resJeu) && res == resExec) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }	

    /**
     * Divides into two piles the matches of a line of play (1 line = 1 heap)
     * 
     * @param jeu   Table of matches by line
     * @param ligne line (heap) on which matches must be separated
     * @param nb    number of matches REMOVED from line after separation
     */
    void enlever ( ArrayList<Integer> jeu, int ligne, int nb ) {
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("enlever() : le paramètre jeu est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("enlever() : le numéro de ligne est trop grand");
        } else if (nb >= jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop grand");
        } else if (nb <= 0) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop petit");
        } else if (2 * nb == jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est la moitié");
        } else {
			// nouveau tas (case) ajouté au jeu (nécessairement en fin de tableau)
			// ce nouveau tas contient le nbre d'allumettes retirées (nb) du tas à séparer			
            jeu.add(nb);
			// le tas restant avec "nb" allumettes en moins
            jeu.set(ligne, jeu.get(ligne) - nb);
        }
    }

    /**
     * Test if it is possible to separate one of the piles
     * 
     * @param jeu      Game board
     * @return true if there is at least one pile of 3 or more matches, false otherwise
     */
    boolean estPossible(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estPossible(): le paramètre jeu est null");
        } else {
            int i = 0;
            while (i < jeu.size() && !ret) {
                if (jeu.get(i) > 2) {
                    ret = true;
                }
                i = i + 1;
            }
        }
        return ret;
    }

    /**
     * Creates a first-ever trial setup from within the game
     * 
     * @param jeu ame board
     * @param jeuEssai New game configuration
     * @return the pile number divided into two or (-1) if there is no pile of at least 3 matches
     */
    int premier(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai) {
	
        int numTas = -1; // pas de tas à séparer par défaut
		int i;
		
        if (jeu == null) {
            System.err.println("premier(): le paramètre jeu est null");
        } else if (!estPossible((jeu)) ){
            System.err.println("premier(): aucun tas n'est divisible");
        } else if (jeuEssai == null) {
            System.err.println("estPossible(): le paramètre jeuEssai est null");
        } else {
            // avant la copie du jeu dans jeuEssai il y a un reset de jeuEssai 
            jeuEssai.clear();
            i = 0;
			
			// recopie case par case
			// jeuEssai est le même que le jeu au départ
            while (i < jeu.size()) {
                jeuEssai.add(jeu.get(i));
                i = i + 1;
            }
			
            i = 0;
			// rechercher un tas d'allumettes d'au moins 3 allumettes dans le jeu
			// sinon numTas = -1
			boolean trouve = false;
            while ( (i < jeu.size()) && !trouve) {
				
				// si on trouve un tas d'au moins 3 allumettes
				if ( jeuEssai.get(i) >= 3 ) {
					trouve = true;
					numTas = i;
				}
				
				i = i + 1;
            }
			
			// sépare le tas (case numTas) en un tas d'UNE SEULE allumette à la fin du tableau 
			// le tas en case numTas a diminué d'une allumette (retrait d'une allumette)
			// jeuEssai est le plateau de jeu qui fait apparaître cette séparation
            if ( numTas != -1 ) enlever ( jeuEssai, numTas, 1 );
        }
		
        return numTas;
    }

    /**
     * Brief tests of the method premier()
     */
    void testPremier() {
        System.out.println();
        System.out.println("*** testPremier()");

        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        jeu1.add(11);
        int ligne1 = 0;
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(9);
        res1.add(11);
        res1.add(1);
        testCasPremier(jeu1, ligne1, res1);
    }

    /**
     * Testing a case of the method testPremier
	 * @param jeu Game board
	 * @param ligne The separate SAT number first
	 * @param res The game board after a first separation
     */
    void testCasPremier(ArrayList<Integer> jeu, int ligne, ArrayList<Integer> res) {
        // Arrange
        System.out.print("premier (" + jeu.toString() + ") : ");
        ArrayList<Integer> jeuEssai = new ArrayList<Integer>();
        // Act
        int noLigne = premier(jeu, jeuEssai);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(res) && noLigne == ligne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }

    /**
     * Generates the following test configuration (i.e. ONE possible decomposition)
     * 
     * @param jeu Game board
     * @param jeuEssai Game trial setup after separation
     * @param ligne    the SAT number that was last to be separated
     * @return the number of the heap divided in half for the new configuration, -1 if no more decomposition is possible
     */
    int suivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne) {
        // System.out.println("suivant(" + jeu.toString() + ", " +jeuEssai.toString() +
        // ", " + ligne + ") = ");
		
		int numTas = -1; // par défaut il n'y a plus de décomposition possible
		
        int i = 0;
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else if (jeuEssai == null) {
            System.err.println("suivant() : le paramètre jeuEssai est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("estPossible(): le paramètre ligne est trop grand");
        }
		
		else {
		
			int nbAllumEnLigne = jeuEssai.get(ligne);
			int nbAllDernCase = jeuEssai.get(jeuEssai.size() - 1);
			
			// si sur la même ligne (passée en paramètre) on peut encore retirer des allumettes,
			// c-à-d si l'écart entre le nombre d'allumettes sur cette ligne et
			// le nombre d'allumettes en fin de tableau est > 2, alors on retire encore
			// 1 allumette sur cette ligne et on ajoute 1 allumette en dernière case		
            if ( (nbAllumEnLigne - nbAllDernCase) > 2 ) {
                jeuEssai.set ( ligne, (nbAllumEnLigne - 1) );
                jeuEssai.set ( jeuEssai.size() - 1, (nbAllDernCase + 1) );
                numTas = ligne;
            } 
			
			// sinon il faut examiner le tas (ligne) suivant du jeu pour éventuellement le décomposer
			// on recrée une nouvelle configuration d'essai identique au plateau de jeu
			else {
                // copie du jeu dans JeuEssai
                jeuEssai.clear();
                for (i = 0; i < jeu.size(); i++) {
                    jeuEssai.add(jeu.get(i));
                }
				
                boolean separation = false;
                i = ligne + 1; // tas suivant
				// si il y a encore un tas et qu'il contient au moins 3 allumettes
				// alors on effectue une première séparation en enlevant 1 allumette
                while ( i < jeuEssai.size() && !separation ) {
					// le tas doit faire minimum 3 allumettes
                    if ( jeu.get(i) > 2 ) {
                        separation = true;
						// on commence par enlever 1 allumette à ce tas
                        enlever(jeuEssai, i, 1);
						numTas = i;
                    } else {
                        i = i + 1;
                    }
                }				
            }
        }
		
        return numTas;
    }

    /**
     * Brief tests of the method suivant()
     */
    void testSuivant() {
        System.out.println();
        System.out.println("*** testSuivant() ****");

        int ligne1 = 0;
        int resLigne1 = 0;
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        ArrayList<Integer> jeuEssai1 = new ArrayList<Integer>();
        jeuEssai1.add(9);
        jeuEssai1.add(1);
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(8);
        res1.add(2);
        testCasSuivant(jeu1, jeuEssai1, ligne1, res1, resLigne1);

        int ligne2 = 0;
        int resLigne2 = -1;
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(10);
        ArrayList<Integer> jeuEssai2 = new ArrayList<Integer>();
        jeuEssai2.add(6);
        jeuEssai2.add(4);
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        res2.add(10);
        testCasSuivant(jeu2, jeuEssai2, ligne2, res2, resLigne2);

        int ligne3 = 1;
        int resLigne3 = 1;
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(4);
        jeu3.add(6);
        jeu3.add(3);
        ArrayList<Integer> jeuEssai3 = new ArrayList<Integer>();
        jeuEssai3.add(4);
        jeuEssai3.add(5);
        jeuEssai3.add(3);
        jeuEssai3.add(1);
        ArrayList<Integer> res3 = new ArrayList<Integer>();
        res3.add(4);
        res3.add(4);
        res3.add(3);
        res3.add(2);
        testCasSuivant(jeu3, jeuEssai3, ligne3, res3, resLigne3);

    }

    /**
     * Testing a case of the method suivant
	 * 
	 * @param jeu Game board
	 * @param jeuEssai the game board obtained after separating a pile
	 * @param ligne the SAT number that was last to be separated
	 * @param resJeu is the gameExpected trial after separation
	 * @param resLigne is the expected number of the heap that is separated
     */
    void testCasSuivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne, ArrayList<Integer> resJeu, int resLigne) {
        // Arrange
        System.out.print("suivant (" + jeu.toString() + ", " + jeuEssai.toString() + ", " + ligne + ") : ");
        // Act
        int noLigne = suivant(jeu, jeuEssai, ligne);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(resJeu) && noLigne == resLigne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
}

