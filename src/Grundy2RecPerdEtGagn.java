import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
    

/**
 * Grundy game
 * 
 * @author A.ROBIN and M.SEVETTE
 */

class Grundy2RecPerdEtGagn{
    long cpt;
    ArrayList<ArrayList<Integer>> posPerdantes = new ArrayList<ArrayList<Integer>>();
    ArrayList<ArrayList<Integer>> posGagnantes = new ArrayList<ArrayList<Integer>>();
    void principal(){
        //testafficheJeu();
        //testPartieTerminee();
        //testNormalise();
        //testIdentiques();
        //testEstConnuePerdante();
        //testEstConnueGagnante();
        //partie();
        testEstGagnanteEfficacite();
    }
    /**
     * Efficiency testing method of estGagnante
     */
    void testEstGagnanteEfficacite(){
        int i = 1;
        int n;
        long t1,t2,diffT;
        // n = 3
        n = 3;
        while( i <= 40){
            posPerdantes.clear();
            posGagnantes.clear();
            ArrayList<Integer> jeu = new ArrayList<Integer>();
            jeu.add(n);
            cpt = 0;
            t1 = System.nanoTime();
            estGagnante(jeu);
            t2 = System.nanoTime();
            diffT = (t2 - t1);//en nanosecondes
            System.out.println(i);
            System.out.println("Tps = " + diffT + "ns");
            System.out.println("l'éfficacité est de " + cpt);
            i++;
            n = n + 1;
        }
    }
    
    
    /**
     * Method that manages the game by creating the players 
     * and the number of matche.
     */
    void partie(){
        String joueur1;
        String joueur2;
        int tour = 0;
        int premier = -1;
        int nbAllumette = SimpleInput.getInt("Nombre d'allumette : ");
        while(nbAllumette <= 2){
            nbAllumette = SimpleInput.getInt("Nombre d'allumette : ");
        }
        joueur1 = SimpleInput.getString("Nom du joueur  : ");
        joueur2 = "Ordi";
        while(premier != 0 && premier != 1){
                premier = SimpleInput.getInt
                ("Premier à jouer :\n- 0 l'ordinateur"+
                "\n- 1 le joueur\n");
        }
        if(premier == 0){
            joueur2 = joueur1;
            joueur1 = "Ordi";
        }
        ArrayList<Integer> jeu = new ArrayList<Integer>();
        jeu.add(nbAllumette);
        while(!partieTerminee(jeu)){
                if (tour % 2 == 0){
                    if(premier == 1){
                    //odd turn it is therefore up to player 2 to play
                        untour(joueur1, jeu, tour);
                    }else{
                        unTourOrdi(joueur1, jeu, tour);
                    }
                }
                if(tour % 2 != 0){
                    //Even tour it is therefore up to the computer to play
                    if(premier == 1){
                        unTourOrdi(joueur2, jeu, tour);
                    }else{
                        untour(joueur2, jeu, tour);
                    }
                }
                tour++;
        }
        if(tour % 2 == 0){
            //If the turn is even then player 1 cannot play
            //and so it is player 2 who wins
            System.out.println(joueur2 + " a gagné.");
        }else{
            System.out.println(joueur1 + " a gagné.");
        }
    }

    /**
     * Method that displays matches
     * @param tour Tour to which we are
     * @param jeu ArrayList filled with matches
     */
    void afficheJeu(int tour, ArrayList<Integer> jeu){
        int i = 0;
        String str = " | ";
        while(i < jeu.size() && i <= tour){
            //repeat() method repeats
            //a number of times a String
            System.out.println
            (i + " : " + str.repeat(jeu.get(i)));
            i++;
        }
    }
    
    /** 
     * Method that checks after each turn that the game can or not continue
     * @param jeu ArrayList filled with matches
     * @return if the game is finish
     */
    boolean partieTerminee(ArrayList<Integer> jeu){
        boolean estTerminee = true;
        int i = 0;
        while(i < jeu.size() && estTerminee){
            if (jeu.get(i) > 2){
                estTerminee = false;
            }
            i++;
        }
        return estTerminee;
    }
    
    /**
     * Method that spins a turn of the game for a given player
     * @param joueur name of the player
     * @param jeu ArrayList filled with matches
     * @param tour Tour to which we are
     */
    void untour(String joueur, ArrayList<Integer> jeu, int tour ){
        int ligne ;
        int nbAllumetteASeparer;
        afficheJeu(tour, jeu);
        System.out.println();
        System.out.println("Tour du joueur : " + joueur);
        ligne = SimpleInput.getInt("Ligne : ");
        //line between 0 and the turn
        while
        (ligne < 0 || ligne > tour || jeu.get(ligne) < 3){
            ligne = SimpleInput.getInt("Ligne : ");
        }
        nbAllumetteASeparer = SimpleInput.getInt
        ("Nombre d'allumettes à séparer : ");
        /**
         * Test if the number of matches is between 1 and the max number
         *  of the line - 1 and finally if the two heaps will be very 
         * different after separation.
         */
        while(nbAllumetteASeparer <= 0 ||
        nbAllumetteASeparer >= jeu.get(ligne) ||
        nbAllumetteASeparer ==  jeu.get(ligne) - nbAllumetteASeparer){
            nbAllumetteASeparer = SimpleInput.getInt
            ("Nombre d'allumettes à séparer : ");
        }
        enlever(jeu,ligne,nbAllumetteASeparer);
    }
    
    /**
     * Method that runs a round of the game for the computer
     * @param joueur name of the player
     * @param jeu ArrayList filled with matches
     * @param tour Tour to which we are
     */
    void unTourOrdi(String joueur, ArrayList<Integer> jeu, int tour){
        int ligne ;
        int nbAllumetteASeparer;
        afficheJeu(tour, jeu);
        System.out.println();
        System.out.println("Tour du joueur : " + joueur);
        if(jouerGagnant(jeu)){
        }else{
            ligne = (int)(Math.random() * tour);
            while(jeu.get(ligne) < 3){
                ligne = (int)(Math.random() * tour);
            }
            nbAllumetteASeparer = (int)(Math.random() * jeu.get(ligne)- 2) + 1;
            while
            (nbAllumetteASeparer == jeu.get(ligne) - nbAllumetteASeparer){
                nbAllumetteASeparer = (int)(Math.random() * jeu.get(ligne)- 2) + 1;
            }
            enlever(jeu,ligne,nbAllumetteASeparer);
        }
    }
    
    /**
     * testing method affichePartie
     */
    void testafficheJeu(){
        System.out.println();
		System.out.println("***testaffichePartie()");
        
        ArrayList<Integer>allumettes1 = new ArrayList<Integer>();
        allumettes1.add(5);
        allumettes1.add(0);
        String result1 = "0 :  |  |  |  |  | ";
		testCasAfficheJeu (0,allumettes1,result1);
        
        ArrayList<Integer>allumettes2 = new ArrayList<Integer>();
        allumettes2.add(5);
        allumettes2.add(4);
        allumettes2.add(0);
        String result2 = "0 :  |  |  |  |  | \n1 :  |  |  |  | ";
		testCasAfficheJeu (1,allumettes2,result2);
        
        ArrayList<Integer>allumettes3 = new ArrayList<Integer>();
        allumettes3.add(1);
        allumettes3.add(1);
        allumettes3.add(3);
        allumettes3.add(2);
        String result3 = "0 :  | \n1 :  | \n2 :  |  |  |\n3 :  |  | ";
		testCasAfficheJeu (3,allumettes3,result3);
    }
    /**
     * Test a call of affichePartie
     * @param tour Tour to which we are
     * @param jeu ArrayList filled with matches
     * @param result expected Result
     */
    void testCasAfficheJeu
    (int tour, ArrayList<Integer> jeu, String result){
        System.out.println("résultat :\n" + result);
        System.out.print("méthode:\n");
        afficheJeu(tour,jeu);
        System.out.println();
        
    }
    
    /**
	 * Tests the partieTerminer method
	 */
	void testPartieTerminee(){
		System.out.println();
		System.out.println("***testpartieTerminer()");
        
		int []tab1 = {1,2,3,4};
        ArrayList<Integer>allumettes1 = new ArrayList<Integer>();
        allumettes1.add(1);
        allumettes1.add(2);
        allumettes1.add(3);
        allumettes1.add(4);
		testCasPartieTerminee (allumettes1,false);
        
        int []tab2 = {1,2,1,2};
        ArrayList<Integer>allumettes2 = new ArrayList<Integer>();
        allumettes2.add(1);
        allumettes2.add(2);
        allumettes2.add(1);
        allumettes2.add(2);
		testCasPartieTerminee (allumettes2,true);
        
        int []tab3 = {3,2,1,2};
        ArrayList<Integer>allumettes3 = new ArrayList<Integer>();
        allumettes3.add(3);
        allumettes3.add(2);
        allumettes3.add(1);
        allumettes3.add(2);
		testCasPartieTerminee (allumettes3,false);
        
        int []tab4 = {1,2,1,3};
        ArrayList<Integer>allumettes4 = new ArrayList<Integer>();
        allumettes4.add(1);
        allumettes4.add(2);
        allumettes4.add(1);
        allumettes4.add(3);
		testCasPartieTerminee (allumettes4,false);
        
        int []tab5 = {1,5,1,3};
        ArrayList<Integer>allumettes5 = new ArrayList<Integer>();
        allumettes5.add(1);
        allumettes5.add(5);
        allumettes5.add(1);
        allumettes5.add(3);
		testCasPartieTerminee (allumettes5,false);
        
        int []tab6 = {5,0,0,0};
        ArrayList<Integer>allumettes6 = new ArrayList<Integer>();
        allumettes6.add(5);
        allumettes6.add(0);
        allumettes6.add(0);
        allumettes6.add(0);
		testCasPartieTerminee (allumettes6,false);
        
	}
	/**
	 * Test a call of partieTerminer
	 * @param jeu ArrayList filled with matches
     * @param result expected Result
	 */
	void testCasPartieTerminee
    (ArrayList<Integer> jeu, boolean result){
		//Arrange
		System.out.print 
		("partieTerminer () \t= " + result + "\t : ");
		
		//Act
		boolean resExec = partieTerminee(jeu);
        
		//Assert
		if (resExec == result){
			System.out.println("OK");
		}else{
			System.out.println("ERREUR");
		}
	}
	
    /**
     * Play the winning move if it exists
     * 
     * @param jeu Game board
     * @return vrai If there is a winning move, false otherwise
     */
    boolean jouerGagnant(ArrayList<Integer> jeu) {
        boolean gagnant = false;
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else {
            ArrayList<Integer> essai = new ArrayList<Integer>();
            int ligne = premier(jeu, essai);
			// mise en oeuvre de la règle numéro2
			// Une situation (ou position) est dite gagnante pour la machine (ou le joueur, peu importe), s’il existe AU MOINS UNE 
			// décomposition (c-à-d UNE action qui consiste à décomposer un tas en 2 tas inégaux) perdante pour l’adversaire.
            while (ligne != -1 && !gagnant) {
                if (estPerdante(essai)) {
                    jeu.clear();
                    gagnant = true;
                    for (int i = 0; i < essai.size(); i++) {
                        jeu.add(essai.get(i));
                    }
                } else {
                    ligne = suivant(jeu, essai, ligne);
                }
            }
        }
		
        return gagnant;
    }
	
	/**
     * RECURSIVE method that indicates whether the configuration (of the current game or trial set) loses
     * 
     * @param jeu Current game board (the state of the game at a certain point during the game)
     * @return vrai If the configuration (of the game) loses, false otherwise
     */
    boolean estPerdante(ArrayList<Integer> jeu) {
        ArrayList<Integer> copie = new ArrayList<Integer>();
        boolean ret = true; // par défaut la configuration est perdante
		
        if (jeu == null) {
            System.err.println("estPerdante(): le paramètre jeu est null");
        }
		
        else {
            // si il n'y a plus que des tas de 1 ou 2 allumettes dans le plateau de jeu
            // alors la situation est forcément perdante (ret=true) = FIN de la récursivité
            if ( !estPossible(jeu) ) {
                ret = true;
            }
            // si le plateau de jeu (passé en paramètre) est CONNU comme étant PERDANT
            // alors on retourne immédiatement "true" et on arrête la récursivité (donc on gagne du temps !)
            else if ( estConnuePerdante ( jeu ) ) {
                ret = true;
            }
            else if ( estConnueGagnante ( jeu ) ) {
				ret = false;
            }
            // sinon il faut poursuivre la décomposition des tas en créant le jeu d'essai suivant
            else {
                // création d'un jeu d'essais qui va examiner toutes les décompositions
                // possibles à partir de jeu
                ArrayList<Integer> essai = new ArrayList<Integer>(); // size = 0
                    
                // toute première décomposition : enlever 1 allumette au premier tas qui possède
                // au moins 3 allumettes, ligne = -1 signifie qu'il n'y a plus de tas d'au moins 3 allumettes
                int ligne = premier(jeu, essai);
                while ( (ligne != -1) && ret) {
                    // mise en oeuvre de la règle numéro1
                    // Une situation (ou position) est dite perdante pour la machine (ou le joueur, peu importe) si et seulement si TOUTES 
                    // ses décompositions possibles (c-à-d TOUTES les actions qui consistent à décomposer un tas en 2 tas inégaux) sont 
                    // TOUTES gagnantes pour l’adversaire.
                    // Si UNE SEULE décomposition (à partir du jeu) est perdante (pour l'adversaire) alors la configuration n'EST PAS perdante.
                    // Ici l'appel à "estPerdante" est RECURSIF.
                    if (estPerdante(essai) == true) {
                        
                        ret = false;
                            
                    } else {
                        // génère la configuration d'essai suivante (c'est-à-dire UNE décomposition possible)
                        // à partir du jeu, si ligne = -1 il n'y a plus de décomposition possible
                        ligne = suivant(jeu, essai, ligne);
                    }
                }
                if ( ret ){
                    copie = normalise(jeu);
                    posPerdantes.add ( copie );
                }
                else{
                    copie = normalise(jeu);
                    posGagnantes.add ( copie );
                }
            }
        }
        //System.out.println("perdant");
        //System.out.println(posPerdantes);
        //System.out.println("gagnant");
        //System.out.println(posGagnantes);
        cpt++;
        return ret;
    }
	
	/**
     * Indicates whether the configuration wins.
	 * Method that simply calls "estPerdante".
     * 
     * @param jeu board game
     * @return vrai si la configuration est gagnante, faux sinon
     */
    boolean estGagnante(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estGagnante(): le paramètre jeu est null");
        } else{
            ret = !estPerdante(jeu);
        }
        return ret;
    }

    /**
     * Tests succincts de la méthode joueurGagnant()
     */
    void testJouerGagnant() {
        System.out.println();
        System.out.println("*** testJouerGagnant() ***");

        System.out.println("Test des cas normaux");
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(6);
        ArrayList<Integer> resJeu1 = new ArrayList<Integer>();
        resJeu1.add(4);
        resJeu1.add(2);
		
        testCasJouerGagnant(jeu1, resJeu1, true);
        
    }

    /**
     * Test d'un cas de la méthode jouerGagnant()
	 *
	 * @param jeu le plateau de jeu
	 * @param resJeu le plateau de jeu après avoir joué gagnant
	 * @param res le résultat attendu par jouerGagnant
     */
    void testCasJouerGagnant(ArrayList<Integer> jeu, ArrayList<Integer> resJeu, boolean res) {
        // Arrange
        System.out.print("jouerGagnant (" + jeu.toString() + ") : ");

        // Act
        boolean resExec = jouerGagnant(jeu);

        // Assert
        System.out.print(jeu.toString() + " " + resExec + " : ");
        if (jeu.equals(resJeu) && res == resExec) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }	

    /**
     * Divise en deux tas les alumettes d'une ligne de jeu (1 ligne = 1 tas)
     * 
     * @param jeu   tableau des alumettes par ligne
     * @param ligne ligne (tas) sur laquelle les alumettes doivent être séparées
     * @param nb    nombre d'alumettes RETIREE de la ligne après séparation
     */
    void enlever ( ArrayList<Integer> jeu, int ligne, int nb ) {
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("enlever() : le paramètre jeu est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("enlever() : le numéro de ligne est trop grand");
        } else if (nb >= jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop grand");
        } else if (nb <= 0) {
            System.err.println("enlever() : le nb d'allumettes à retirer est trop petit");
        } else if (2 * nb == jeu.get(ligne)) {
            System.err.println("enlever() : le nb d'allumettes à retirer est la moitié");
        } else {
			// nouveau tas (case) ajouté au jeu (nécessairement en fin de tableau)
			// ce nouveau tas contient le nbre d'allumettes retirées (nb) du tas à séparer			
            jeu.add(nb);
			// le tas restant avec "nb" allumettes en moins
            jeu.set(ligne, jeu.get(ligne) - nb);
        }
    }

    /**
     * Teste s'il est possible de séparer un des tas
     * 
     * @param jeu      plateau de jeu
     * @return vrai s'il existe au moins un tas de 3 allumettes ou plus, faux sinon
     */
    boolean estPossible(ArrayList<Integer> jeu) {
        boolean ret = false;
        if (jeu == null) {
            System.err.println("estPossible(): le paramètre jeu est null");
        } else {
            int i = 0;
            while (i < jeu.size() && !ret) {
                if (jeu.get(i) > 2) {
                    ret = true;
                }
                i = i + 1;
            }
        }
        return ret;
    }

    /**
     * Crée une toute première configuration d'essai à partir du jeu
     * 
     * @param jeu      plateau de jeu
     * @param jeuEssai nouvelle configuration du jeu
     * @return le numéro du tas divisé en deux ou (-1) si il n'y a pas de tas d'au moins 3 allumettes
     */
    int premier(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai) {
	
        int numTas = -1; // pas de tas à séparer par défaut
		int i;
		
        if (jeu == null) {
            System.err.println("premier(): le paramètre jeu est null");
        } else if (!estPossible((jeu)) ){
            System.err.println("premier(): aucun tas n'est divisible");
        } else if (jeuEssai == null) {
            System.err.println("estPossible(): le paramètre jeuEssai est null");
        } else {
            // avant la copie du jeu dans jeuEssai il y a un reset de jeuEssai 
            jeuEssai.clear();
            i = 0;
			
			// recopie case par case
			// jeuEssai est le même que le jeu au départ
            while (i < jeu.size()) {
                jeuEssai.add(jeu.get(i));
                i = i + 1;
            }
			
            i = 0;
			// rechercher un tas d'allumettes d'au moins 3 allumettes dans le jeu
			// sinon numTas = -1
			boolean trouve = false;
            while ( (i < jeu.size()) && !trouve) {
				
				// si on trouve un tas d'au moins 3 allumettes
				if ( jeuEssai.get(i) >= 3 ) {
					trouve = true;
					numTas = i;
				}
				
				i = i + 1;
            }
			
			// sépare le tas (case numTas) en un tas d'UNE SEULE allumette à la fin du tableau 
			// le tas en case numTas a diminué d'une allumette (retrait d'une allumette)
			// jeuEssai est le plateau de jeu qui fait apparaître cette séparation
            if ( numTas != -1 ) enlever ( jeuEssai, numTas, 1 );
        }
		
        return numTas;
    }

    /**
     * Tests succincts de la méthode premier()
     */
    void testPremier() {
        System.out.println();
        System.out.println("*** testPremier()");

        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        jeu1.add(11);
        int ligne1 = 0;
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(9);
        res1.add(11);
        res1.add(1);
        testCasPremier(jeu1, ligne1, res1);
    }

    /**
     * Test un cas de la méthode testPremier
	 * @param jeu le plateau de jeu
	 * @param ligne le numéro du tas séparé en premier
	 * @param res le plateau de jeu après une première séparation
     */
    void testCasPremier(ArrayList<Integer> jeu, int ligne, ArrayList<Integer> res) {
        // Arrange
        System.out.print("premier (" + jeu.toString() + ") : ");
        ArrayList<Integer> jeuEssai = new ArrayList<Integer>();
        // Act
        int noLigne = premier(jeu, jeuEssai);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(res) && noLigne == ligne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }

    /**
     * Génère la configuration d'essai suivante (c'est-à-dire UNE décomposition possible)
     * 
     * @param jeu      plateau de jeu
     * @param jeuEssai configuration d'essai du jeu après séparation
     * @param ligne    le numéro du tas qui est le dernier à avoir été séparé
     * @return le numéro du tas divisé en deux pour la nouvelle configuration, -1 si plus aucune décomposition n'est possible
     */
    int suivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne) {
	
        // System.out.println("suivant(" + jeu.toString() + ", " +jeuEssai.toString() +
        // ", " + ligne + ") = ");
		
		int numTas = -1; // par défaut il n'y a plus de décomposition possible
		
        int i = 0;
		// traitement des erreurs
        if (jeu == null) {
            System.err.println("suivant(): le paramètre jeu est null");
        } else if (jeuEssai == null) {
            System.err.println("suivant() : le paramètre jeuEssai est null");
        } else if (ligne >= jeu.size()) {
            System.err.println("estPossible(): le paramètre ligne est trop grand");
        }
		
		else {
		
			int nbAllumEnLigne = jeuEssai.get(ligne);
			int nbAllDernCase = jeuEssai.get(jeuEssai.size() - 1);
			
			// si sur la même ligne (passée en paramètre) on peut encore retirer des allumettes,
			// c-à-d si l'écart entre le nombre d'allumettes sur cette ligne et
			// le nombre d'allumettes en fin de tableau est > 2, alors on retire encore
			// 1 allumette sur cette ligne et on ajoute 1 allumette en dernière case		
            if ( (nbAllumEnLigne - nbAllDernCase) > 2 ) {
                jeuEssai.set ( ligne, (nbAllumEnLigne - 1) );
                jeuEssai.set ( jeuEssai.size() - 1, (nbAllDernCase + 1) );
                numTas = ligne;
            } 
			
			// sinon il faut examiner le tas (ligne) suivant du jeu pour éventuellement le décomposer
			// on recrée une nouvelle configuration d'essai identique au plateau de jeu
			else {
                // copie du jeu dans JeuEssai
                jeuEssai.clear();
                for (i = 0; i < jeu.size(); i++) {
                    jeuEssai.add(jeu.get(i));
                }
				
                boolean separation = false;
                i = ligne + 1; // tas suivant
				// si il y a encore un tas et qu'il contient au moins 3 allumettes
				// alors on effectue une première séparation en enlevant 1 allumette
                while ( i < jeuEssai.size() && !separation ) {
					// le tas doit faire minimum 3 allumettes
                    if ( jeu.get(i) > 2 ) {
                        separation = true;
						// on commence par enlever 1 allumette à ce tas
                        enlever(jeuEssai, i, 1);
						numTas = i;
                    } else {
                        i = i + 1;
                    }
                }				
            }
        }
		
        return numTas;
    }

    /**
     * Tests succincts de la méthode suivant()
     */
    void testSuivant() {
        System.out.println();
        System.out.println("*** testSuivant() ****");

        int ligne1 = 0;
        int resLigne1 = 0;
        ArrayList<Integer> jeu1 = new ArrayList<Integer>();
        jeu1.add(10);
        ArrayList<Integer> jeuEssai1 = new ArrayList<Integer>();
        jeuEssai1.add(9);
        jeuEssai1.add(1);
        ArrayList<Integer> res1 = new ArrayList<Integer>();
        res1.add(8);
        res1.add(2);
        testCasSuivant(jeu1, jeuEssai1, ligne1, res1, resLigne1);

        int ligne2 = 0;
        int resLigne2 = -1;
        ArrayList<Integer> jeu2 = new ArrayList<Integer>();
        jeu2.add(10);
        ArrayList<Integer> jeuEssai2 = new ArrayList<Integer>();
        jeuEssai2.add(6);
        jeuEssai2.add(4);
        ArrayList<Integer> res2 = new ArrayList<Integer>();
        res2.add(10);
        testCasSuivant(jeu2, jeuEssai2, ligne2, res2, resLigne2);

        int ligne3 = 1;
        int resLigne3 = 1;
        ArrayList<Integer> jeu3 = new ArrayList<Integer>();
        jeu3.add(4);
        jeu3.add(6);
        jeu3.add(3);
        ArrayList<Integer> jeuEssai3 = new ArrayList<Integer>();
        jeuEssai3.add(4);
        jeuEssai3.add(5);
        jeuEssai3.add(3);
        jeuEssai3.add(1);
        ArrayList<Integer> res3 = new ArrayList<Integer>();
        res3.add(4);
        res3.add(4);
        res3.add(3);
        res3.add(2);
        testCasSuivant(jeu3, jeuEssai3, ligne3, res3, resLigne3);

    }

    /**
     * Test un cas de la méthode suivant
	 * 
	 * @param jeu le plateau de jeu
	 * @param jeuEssai le plateau de jeu obtenu après avoir séparé un tas
	 * @param ligne le numéro du tas qui est le dernier à avoir été séparé
	 * @param resJeu est le jeuEssai attendu après séparation
	 * @param resLigne est le numéro attendu du tas qui est séparé
     */
    void testCasSuivant(ArrayList<Integer> jeu, ArrayList<Integer> jeuEssai, int ligne, ArrayList<Integer> resJeu, int resLigne) {
        // Arrange
        System.out.print("suivant (" + jeu.toString() + ", " + jeuEssai.toString() + ", " + ligne + ") : ");
        // Act
        int noLigne = suivant(jeu, jeuEssai, ligne);
        // Assert
        System.out.println("\nnoLigne = " + noLigne + " jeuEssai = " + jeuEssai.toString());
        if (jeuEssai.equals(resJeu) && noLigne == resLigne) {
            System.out.println("OK\n");
        } else {
            System.err.println("ERREUR\n");
        }
    }
    
    /**
     * Determines whether the configuration is known as a loser in posPerdante
     * 
     * @param jeu le plateau de jeu
     * @return vrai si le jeu est dans posPerdantes
     */
    boolean estConnuePerdante ( ArrayList<Integer> jeu ) {
        // création d'une copie de jeu triée sans 1, ni 2
        ArrayList<Integer> copie = normalise(jeu);
        boolean ret = false;
        int i = 0;
        while ( !ret && i < posPerdantes.size() ) {
            if (identiques(copie, posPerdantes.get(i))) {
                ret = true;
            }
            i++;
        }
        return ret;
    }
    /**
     * method that copies an ArrayList
     * @param jeu game to copy
     * @return the copy
     */
    ArrayList<Integer> normalise(ArrayList<Integer> jeu){
        ArrayList<Integer> copie = new ArrayList<Integer>();
        int i = 0;
        while(i < jeu.size()){
            if(jeu.get(i) > 2)copie.add(jeu.get(i));
            
            i++;
        }
        return copie;
    }
    /**
	 * Method allowing do determine if two lists contan the same items
	 * @param liste1 first list
	 * @param liste2 second list
	 * @return true if lists are identical, false otherwise
	 */
	boolean identiques(ArrayList<Integer> liste1, ArrayList<Integer> liste2){
        boolean ret = false;
        if (liste1.size() == liste2.size()) {
            Collections.sort(liste1);
            Collections.sort(liste2);
            ret = true;
            int i = 0;
            while (ret && i < liste1.size()) {
                if (liste1.get(i) != liste2.get(i)) {
                    ret = false;
                }
                i++;
            }
        }
        return ret;
    }
    
    /**
     * Determines whether the configuration is known as a loser in posPerdante
     * 
     * @param jeu le plateau de jeu
     * @return vrai si le jeu est dans posPerdantes
     */
    boolean estConnueGagnante ( ArrayList<Integer> jeu ) {
        // création d'une copie de jeu triée sans 1, ni 2
        ArrayList<Integer> copie = normalise(jeu);
        boolean ret = false;
        int i = 0;
        while ( !ret && i < posGagnantes.size() ) {
            if (identiques(copie, posGagnantes.get(i))) {
                ret = true;
            }
            i++;
        }
        return ret;
    }
    
    /**
     * Allows to test identiques
     */
    void testIdentiques(){
		System.out.println();
        System.out.println("testIdentiques : ");
        ArrayList<Integer> liste1 = new ArrayList<Integer>();
        ArrayList<Integer> liste2 = new ArrayList<Integer>();

        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(9);
        liste2.add(8);
        liste2.add(7);
        testCasIdentiques(liste1,liste2,true);
        
        liste1.clear();
        liste2.clear();
        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(7);
        liste2.add(8);
        liste2.add(9);
        testCasIdentiques(liste1,liste2,true);
        
        liste1.clear();
        liste2.clear();
        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(7);
        liste2.add(8);
        liste2.add(7);
        testCasIdentiques(liste1,liste2,false);
        
        
        liste1.clear();
        liste2.clear();
        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(9);
        liste2.add(8);
        liste2.add(7);
        liste2.add(1);
        testCasIdentiques(liste1,liste2,false);
    }
    
    /**
     * Allows to test several cases of identiques
     * @param liste1 collection of items
     * @param liste2 collection of loosing positions
     * @param ret boolean expected
     */
    void testCasIdentiques(ArrayList<Integer> liste1, ArrayList<Integer> liste2 ,boolean ret){
        // Arrange
        System.out.println ("Copie de la liste : " + liste1.toString() +" Liste à comparer : " + liste2.toString());
        System.out.println("Résulat attendu : " +ret);
		// Act
        boolean resExec;
        resExec = identiques(liste1,liste2);
        // Assert			 
        if ( resExec == ret ){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /**
	 * Method allowing to test normalise method
	 */
	void testNormalise(){
		System.out.println();
		System.out.println("testNormalise : ");
		ArrayList<Integer>liste1 = new ArrayList<Integer>();
		ArrayList<Integer>liste2 = new ArrayList<Integer>();
		liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(9);
        liste2.add(8);
        liste2.add(7);
		testCasNormalise(liste1,liste2);
        
		liste1.clear();
        liste2.clear();
        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste2.add(9);
        liste2.add(8);
        liste2.add(7);
        liste1.add(1);
        testCasNormalise(liste1,liste2);
        
        liste1.clear();
        liste2.clear();
        liste1.add(9);
        liste1.add(8);
        liste1.add(7);
        liste1.add(1);
        liste1.add(2);
        liste1.add(3);
        liste2.add(9);
        liste2.add(8);
        liste2.add(7);
        liste2.add(3);
        testCasNormalise(liste1,liste2);
        
        liste1.clear();
        liste2.clear();
        liste1.add(1);
        liste1.add(2);
        liste1.add(1);
        testCasNormalise(liste1,liste2);
	}
	
	/**
	 * Method allowing to test several cases of normalise method
	 * @param jeu game board
	 * @param result list expected after normalise method
	 */
	void testCasNormalise(ArrayList<Integer> jeu, ArrayList<Integer> result){
        // Arrange
        System.out.println ("Liste de départ (" + jeu.toString() +", résultat attendu : "+ result.toString()+")");
		// Act
        ArrayList<Integer> resExec = new ArrayList<Integer>();
        resExec = normalise(jeu);
        System.out.println("Résultat : " + resExec.toString());
        // Assert			 
        if (identiques(resExec,result)){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /**
     * Method allowing to test estConnuePerdante method
     */
    void testEstConnuePerdante() {
		System.out.println();
        System.out.println("*** testEstConnuePerdante : ");
        // Basic cases
        System.out.println("estConnuePerdante n°1 : ");
        ArrayList<Integer> c1 = new ArrayList<Integer>();
		c1.add(7);
        c1.add(10);
        c1.add(4);
        posPerdantes.add(c1);
        testCasEstConnuePerdante(c1,true);
        posPerdantes.clear();
        System.out.println("estConnuePerdante n°2 : ");
        ArrayList<Integer> c2 = new ArrayList<Integer>();
        c2.add(90);
        c2.add(40);
        c2.add(100);
        posPerdantes.add(c1);
        testCasEstConnuePerdante(c2,false);
    }

    /**
     * Method allowing to test several cases of estConnuePerdante method
     * @param copie collection of integers items
     * @param ret boolean expected after method
     */
    void testCasEstConnuePerdante(ArrayList<Integer> copie,boolean ret ){
        // Arrange
        System.out.println ("copie de la liste : " + copie.toString()+ " Resultat attendu :  " + ret ) ;
        // Act
        boolean resExec;
        resExec = estConnuePerdante(copie);
        System.out.println("Liste des situations perdantes : " + posPerdantes.toString());
        // Assert			 
        if ( resExec == ret ){
            System.out.println ("OK");
        } else {
            System.err.println ("ERREUR");
        }
    }
    
    /**
     * Method allowing to test estConnueGagnante
     */
    void testEstConnueGagnante() {
		System.out.println();
        System.out.println("*** test estConnueGagnante : ");
        ArrayList<Integer> c1 = new ArrayList<Integer>();
        c1.add(4);
        c1.add(9);
        c1.add(1);
        c1.add(6);
        posGagnantes.add(normalise(c1));
        testCasEstConnueGagnante(c1,true);
        posGagnantes.clear();
        ArrayList<Integer> c2 = new ArrayList<Integer>();
        c2.add(9);
        c2.add(6);
        posGagnantes.add(normalise(c1));
        testCasEstConnueGagnante(c2,false);
    }

    /**
     * Allows to test several cases of estConnueGagnante
     * @param copie collection of items
     * @param ret boolean expected after estConnueGagnante
     */
    void testCasEstConnueGagnante(ArrayList<Integer> copie,boolean ret ){
		// Arrange
		System.out.println ("copie de la liste : " + copie.toString()+ " Resultat attendu :  "+ ret );
		// Act
		boolean resExec;
		resExec = estConnueGagnante(copie);
		System.out.println("Liste de situations gagnantes : " + posGagnantes.toString());
		// Assert			 
		if ( resExec == ret ){
			System.out.println ("OK");
		} else {
			System.err.println ("ERREUR");
		}
    }
}
